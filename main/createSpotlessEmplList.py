# Author by James Fitch
# Created 4/03/2021
# Purpose is to 
# 1. take email attachment from Spotless and save in input
# 2. un-zip file if sent as .zip
# 3. merge data on all worksheets of spreadsheet to one worksheet
# 4. save as a csv in output location
# 
import pandas as pd
#import numpy as np
import datetime as dt
import os
import win32com.client
import zipfile

# global vars
# file paths
cwd = os.getcwd()
dateTimeObj = dt.datetime.now()
timestampStr = dateTimeObj.strftime("%Y%m%dT%H%M%S")

inputDirPath = cwd + '\input'
inputFilePath = inputDirPath + '\\'
outputDirPath = 'output'
outputFilePath = r'\\afinsftp01\Finance\Incoming\Spotless\Spotless_EmployeeExport'+ timestampStr + '.csv'

def container():

    saveAttachments()
    if len(os.listdir(inputDirPath)) > 0: 
    
        for item in os.listdir(inputDirPath): # loop through items in dir
            inputFilePath = inputDirPath + '\\' +  str(item)
        
        handleMergeAndCleanseSheets(inputFilePath)

        os.remove(inputFilePath)
    else:
        print('no files in the input dir to cleanse')
    
def handleMergeAndCleanseSheets(inputFile):
    print('merging tabs...')
    sheets_dict = pd.read_excel(inputFile, sheet_name=None)

    df = pd.DataFrame()
    for name, sheet in sheets_dict.items():
        sheet['location'] = name.strip()
        sheet['company'] = 'Spotless'
        sheet = sheet.rename(columns=lambda x: x.split('\n')[-1])
        df = df.append(sheet)

    df.reset_index(inplace=True, drop=True)

    #MARK - data cleansing
     # replace empty employee IDs with blank then drop the blanks. rows with no employee number are empty rows
    df['Employee ID'].fillna("blank", inplace = True)
    df = df[df['Employee ID'] != 'blank'] 
    
    # replace the empty name variations with blank
    df['First '].fillna("blank", inplace = True)
    df['First Name'].fillna("blank", inplace = True)
    df['First Name '].fillna("blank", inplace = True)
    
    df['Last '].fillna("blank", inplace = True)
    df['Last Name'].fillna("blank", inplace = True)
    df['Last Name '].fillna("blank", inplace = True)
    
    # iterate through each row and merge the possible name fields into one
    for index in df.index:
        firstName = cleanseNames(df['First '][index], df['First Name'][index], df['First Name '][index])
        lastName = cleanseNames(df['Last '][index], df['Last Name'][index], df['Last Name '][index])
        df['First '][index] = firstName.strip()
        df['Last '][index] = lastName.strip()

    # delete the trash columns
    df['first'] = df['First ']
    df['last'] = df['Last ']

    # remove any columns that are not in my final list
    final_table_columns = ['first', 'last','Employee ID','Phone','DOB','location','company']
    df = df[df.columns.intersection(final_table_columns)]

    # change data type to an int to remove .0 on empl id
    df['Employee ID'] = df['Employee ID'].astype(int, errors='ignore')

    # export to csv
    df.to_csv(outputFilePath, index=False)  

    print('done')

def cleanseNames(first, second, third):
    cleansedName = ''

    if third != 'blank':
        cleansedName = third
    elif second != 'blank':
        cleansedName = second
    else:
        cleansedName = first

    return cleansedName


# MARK - Outlook methods
def saveAttachments():
    # local vars
    outlook = win32com.client.Dispatch('Outlook.Application').GetNamespace('MAPI')
    inbox = outlook.GetDefaultFolder(6) 
    spotlessFolder = inbox.Folders['DAS'].Folders['Fit Testing'].Folders['Spotless_Empl_Files']
    messages = spotlessFolder.Items
    sender = 'Matthew.Elkins@SPOTLESSAU1PROD.onmicrosoft.com'
    extension = '.zip'

    for message in messages:
        attachments = message.Attachments
        
        if message.SenderEmailAddress == sender and message.Unread and len(attachments) > 0:
            
            for attachment in attachments:
                fileName = str(attachment)
                if extension in fileName or '.xlsx' in fileName or '.xls' in fileName or '.csv' in fileName: # just in case we get strange formats
                    attachment.SaveAsFile(os.path.join(inputDirPath, fileName))
                    if extension in fileName:
                        fullDir = inputDirPath + '\\'+  fileName
                        unzipAttachments(fullDir)

            message.Unread = False
            print('saved attachment')


def unzipAttachments(fullDir):
    zip_ref = zipfile.ZipFile(fullDir) # create zipfile object
    zip_ref.extractall(inputDirPath) # extract file to dir
    zip_ref.close() # close file
    os.remove(fullDir) # delete zipped file


container()