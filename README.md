# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* 1.0 Created 11/03/2021
* Purpose is to 
1. take email attachment from Spotless and save in input
2. un-zip file if sent as .zip
3. merge data on all worksheets of spreadsheet to one worksheet
4. save as a csv in output location


### How do I get set up? ###

* Clone repo to local machine
* Edit mailbox path to where you store the Spotless files
* ensure python frameworks are installed. python -m pip install [framework] if you are missing any 

### Contribution guidelines ###

* Feel free to open a branch and make contributions

### Who do I talk to? ###

* James Fitch